package services;

import java.util.List;

import materialien.Meal;

/**
 * handles meal related things
 * @author florian
 *
 */
public class MealService {
	
	private List<Meal> _mealList;
	
	/**
	 * creates a mealservice, which handles meal related task, such as adding, removing or editing meals.
	 */
	public MealService()
	{
		
	}
	
	/**
	 * generates a meal plan for the given number of days
	 * @param days number for how any days the plan should be
	 * @return ordered list of meals
	 */
	public List<Meal> generatePlan(int days)
	{
		return null;
	}
	
	
	
	

}
