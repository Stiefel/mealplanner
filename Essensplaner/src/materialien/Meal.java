package materialien;


/**
 * represents a dish with following informations: name, ingredients, preperation, cost and calories
 * @author florian
 *
 */
public interface Meal {
	/**
	 * returns the name of meal in a 'printable' form
	 * @return name of meal
	 */
	String getName();
	
	/**
	 * returns the ingredients of the meal as a string
	 * @return ingredients of the meal
	 */
	String getIngredients();
	
	/**
	 * returns the preperation of the meal
	 * @return preperation of the meal
	 */
	String getPreperation();
	
	/**
	 * returns the approximate cost of the meal
	 * @return cost of the meal
	 */
	double getCost();
	
	/**
	 * returns the approximate calories of the meal
	 * @return calories of the meal
	 */
	int getCalories();

}
